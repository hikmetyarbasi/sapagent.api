﻿using System;
using System.Collections.Generic;
using System.Text;
using SapAgent.DataAccess.Abstract;
using SapAgent.DataAccess.Concrete.EntityFramework.General;
using SapAgent.Entities.Concrete.Pure;

namespace SapAgent.DataAccess.Concrete.EntityFramework.Pure
{
    public class ReplicDal:BaseDal<Replic>
    {
        public ReplicDal(IEntityRepository<Replic> entityRepository) : base(entityRepository)
        {
        }
    }
}
