﻿using System;
using System.Collections.Generic;
using System.Text;
using SapAgent.DataAccess.Abstract;
using SapAgent.DataAccess.Concrete.EntityFramework.General;
using SapAgent.Entities.Concrete.Admin;

namespace SapAgent.DataAccess.Concrete.EntityFramework.Admin
{
    public class TableDataCountDal:BaseDal<TableDataCount>
    {
        public TableDataCountDal(IEntityRepository<TableDataCount> entityRepository) : base(entityRepository)
        {
        }
    }
}
