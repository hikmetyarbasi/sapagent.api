﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SapAgent.Entities.Concrete;

namespace Helpers.Abstract
{
    public interface IHttpClientHelper<T, K>
    {
        Task<K> GetSingleItemRequest(string apiUrl, CancellationToken token = default(CancellationToken));
        Task<K[]> GetMultipleItemsRequest(string apiUrl, CancellationToken token = default(CancellationToken));
        Task<K> PostRequest(string apiUrl, T postObject, CancellationToken token = default(CancellationToken));
        Task PutRequest(string apiUrl, T putObject, CancellationToken token = default(CancellationToken));
        Task DeleteRequest(string apiUrl, CancellationToken token = default(CancellationToken));

    }
}
