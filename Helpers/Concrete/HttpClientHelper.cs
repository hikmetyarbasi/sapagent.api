﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Helpers.Abstract;
using Newtonsoft.Json;
using SapAgent.Entities.Concrete;

namespace Helpers.Concrete
{
    public class HttpClientHelper<T, K> : IHttpClientHelper<T, K>
    {
        private static readonly HttpClient Client = new HttpClient();
        private static readonly string _baseUrl = "http://api.sapagent.com/api/";
        /// <summary>
        /// For getting a single item from a web api uaing GET
        /// </summary>
        /// <param name="apiUrl">Added to the base address to make the full url of the 
        ///     api get method, e.g. "products/1" to get a product with an id of 1</param>
        /// <param name="cancellationToken"></param>
        /// <returns>The item requested</returns>
        public async Task<K> GetSingleItemRequest(string apiUrl, CancellationToken cancellationToken)
        {
            var resp= default(K);
            var response = await Client.GetAsync(_baseUrl + apiUrl, cancellationToken).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                await response.Content.ReadAsStringAsync().ContinueWith(x =>
                {
                 resp = JsonConvert.DeserializeObject<K>(x?.Result);
                }, cancellationToken);
            }
            else
            {
                var content = await response.Content.ReadAsStringAsync();
                //response.Content?.Dispose();
                throw new HttpRequestException($"{response.StatusCode}:{content}");
            }
            return resp;
        }

        /// <summary>
        /// For getting multiple (or all) items from a web api using GET
        /// </summary>
        /// <param name="apiUrl">Added to the base address to make the full url of the 
        ///     api get method, e.g. "products?page=1" to get page 1 of the products</param>
        /// <param name="cancellationToken"></param>
        /// <returns>The items requested</returns>
        public async Task<K[]> GetMultipleItemsRequest(string apiUrl, CancellationToken cancellationToken)
        {
            try
            {
                var resp = default(K[]);
                var response = await Client.GetAsync(_baseUrl + apiUrl, cancellationToken).ConfigureAwait(false);
                if (response.IsSuccessStatusCode)
                {
                    await response.Content.ReadAsStringAsync().ContinueWith((Task<string> x) =>
                    {
                        resp = JsonConvert.DeserializeObject<K[]>(x.Result);
                    }, cancellationToken);
                }
                else
                {
                    var content = await response.Content.ReadAsStringAsync();
                    // response.Content?.Dispose();
                    throw new HttpRequestException($"{response.StatusCode}:{content}");
                }
                return resp;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        /// For creating a new item over a web api using POST
        /// </summary>
        /// <param name="apiUrl">Added to the base address to make the full url of the 
        ///     api post method, e.g. "products" to add products</param>
        /// <param name="postObject">The object to be created</param>
        /// <param name="cancellationToken"></param>
        /// <returns>The item created</returns>
        public async Task<K> PostRequest(string apiUrl, T postObject, CancellationToken cancellationToken)
        {
            var resp = default(K);
            var response = await Client.PostAsync(_baseUrl + apiUrl, new JsonContent(postObject), cancellationToken).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                await response.Content.ReadAsStringAsync().ContinueWith((Task<string> x) =>
                {
                    resp = JsonConvert.DeserializeObject<K>(x.Result);
                }, cancellationToken);
            }
            else
            {
                var content = await response.Content.ReadAsStringAsync();
                //response.Content?.Dispose();
                throw new HttpRequestException($"{response.StatusCode}:{content}");
            }
            return resp;
        }

        /// <summary>
        /// For updating an existing item over a web api using PUT
        /// </summary>
        /// <param name="apiUrl">Added to the base address to make the full url of the 
        ///     api put method, e.g. "products/3" to update product with id of 3</param>
        /// <param name="putObject">The object to be edited</param>
        /// <param name="cancellationToken"></param>
        public async Task PutRequest(string apiUrl, T putObject, CancellationToken cancellationToken)
        {
            var response = await Client.PutAsync(_baseUrl + apiUrl, new JsonContent(putObject)).ConfigureAwait(false);
            if (!response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                //response.Content?.Dispose();
                throw new HttpRequestException($"{response.StatusCode}:{content}");
            }
        }

        /// <summary>
        /// For deleting an existing item over a web api using DELETE
        /// </summary>
        /// <param name="apiUrl">Added to the base address to make the full url of the 
        ///     api delete method, e.g. "products/3" to delete product with id of 3</param>
        /// <param name="cancellationToken"></param>
        public async Task DeleteRequest(string apiUrl, CancellationToken cancellationToken)
        {
            var response = await Client.DeleteAsync(_baseUrl + apiUrl, cancellationToken).ConfigureAwait(false);
            if (!response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                //response.Content?.Dispose();
                throw new HttpRequestException($"{response.StatusCode}:{content}");
            }
        }
    }
}
