﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Text;
using SapAgent.Entities.Abstract;

namespace SapAgent.Entities.Concrete.Pure
{
    [Table("Replic", Schema = "Pure")]
    public class Replic : IEntity
    {
        [Key]
        public int ID { get; set; }
        public string HOST { get; set; }
        [Column("PORT")]
        public string PORT { get; set; }
        [Column("VOLUMEID")]
        public string VOLUME_ID { get; set; }
        [Column("SITEID")]
        public string SITE_ID { get; set; }
        [Column("SITENAME")]
        public string SITE_NAME { get; set; }
        [Column("SECONDARYHOST")]
        public string SECONDARY_HOST { get; set; }
        [Column("SECONDARYPORT")]
        public string SECONDARY_PORT { get; set; }
        [Column("SECONDARYSITEID")]
        public string SECONDARY_SITE_ID { get; set; }
        [Column("SECONDARYSITENAME")]
        public string SECONDARY_SITE_NAME { get; set; }
        [Column("SECONDARYACTIVESTATUS")]
        public string SECONDARY_ACTIVE_STATUS { get; set; }
        [Column("SECONDARYCONNECTTIME")]
        public string SECONDARY_CONNECT_TIME { get; set; }
        [Column("SECONDARYRECONNECTCOUNT")]
        public string SECONDARY_RECONNECT_COUNT { get; set; }
        [Column("SECONDARYFAILOVERCOUNT")]
        public string SECONDARY_FAILOVER_COUNT { get; set; }
        [Column("SECONDARYFULLYRECOVERABLE")]
        public string SECONDARY_FULLY_RECOVERABLE { get; set; }
        [Column("REPLICATIONMODE")]
        public string REPLICATION_MODE { get; set; }
        [Column("REPLICATIONSTATUS")]
        public string REPLICATION_STATUS { get; set; }
        [Column("REPLICATIONSTATUSDETAILS")]
        public string REPLICATION_STATUS_DETAILS { get; set; }
        [Column("FULLSYNC")]
        public string FULL_SYNC { get; set; }
        [Column("LASTLOGPOSITION")]
        public string LAST_LOG_POSITION { get; set; }
        [Column("LASTLOGPOSITIONTIME")]
        public string LAST_LOG_POSITION_TIME { get; set; }
        [Column("LASTSAVEPOINTVERSION")]
        public string LAST_SAVEPOINT_VERSION { get; set; }
        [Column("LASTSAVEPOINTLOGPOSITION")]
        public string LAST_SAVEPOINT_LOG_POSITION { get; set; }
        [Column("LASTSAVEPOINTSTARTTIME")]
        public string LAST_SAVEPOINT_START_TIME { get; set; }
        [Column("SHIPPEDLOGPOSITION")]
        public string SHIPPED_LOG_POSITION { get; set; }
        [Column("SHIPPEDLOGPOSITIONTIME")]
        public string SHIPPED_LOG_POSITION_TIME { get; set; }
        [Column("SHIPPEDLOGBUFFERSCOUNT")]
        public string SHIPPED_LOG_BUFFERS_COUNT { get; set; }
        [Column("SHIPPEDLOGBUFFERSSIZE")]
        public string SHIPPED_LOG_BUFFERS_SIZE { get; set; }
        [Column("SHIPPEDLOGBUFFERSDURATION")]
        public string SHIPPED_LOG_BUFFERS_DURATION { get; set; }
        [Column("SHIPPEDSAVEPOINTVERSION")]
        public string SHIPPED_SAVEPOINT_VERSION { get; set; }
        [Column("SHIPPEDSAVEPOINTLOGPOSITION")]
        public string SHIPPED_SAVEPOINT_LOG_POSITION { get; set; }
        [Column("SHIPPEDSAVEPOINTSTARTTIME")]
        public string SHIPPED_SAVEPOINT_START_TIME { get; set; }
        [Column("SHIPPEDFULLREPLICACOUNT")]
        public string SHIPPED_FULL_REPLICA_COUNT { get; set; }
        [Column("SHIPPEDFULLREPLICASIZE")]
        public string SHIPPED_FULL_REPLICA_SIZE { get; set; }
        [Column("SHIPPEDFULLREPLICADURATION")]
        public string SHIPPED_FULL_REPLICA_DURATION { get; set; }
        [Column("SHIPPEDLASTFULLREPLICASIZE")]
        public string SHIPPED_LAST_FULL_REPLICA_SIZE { get; set; }
        [Column("SHIPPEDLASTFULLREPLICASTAR")]
        public string SHIPPED_LAST_FULL_REPLICA_STAR { get; set; }
        [Column("SHIPPEDLASTFULLREPLICAEND")]
        public string SHIPPED_LAST_FULL_REPLICA_END { get; set; }
        [Column("SHIPPEDDELTAREPLICACOUNT")]
        public string SHIPPED_DELTA_REPLICA_COUNT { get; set; }
        [Column("SHIPPEDDELTAREPLICASIZE")]
        public string SHIPPED_DELTA_REPLICA_SIZE { get; set; }
        [Column("SHIPPEDDELTAREPLICADURATION")]
        public string SHIPPED_DELTA_REPLICA_DURATION { get; set; }
        [Column("SHIPPEDLASTDELTAREPLICASIZ")]
        public string SHIPPED_LAST_DELTA_REPLICA_SIZ { get; set; }
        [Column("SHIPPEDLASTDELTAREPLICASTA")]
        public string SHIPPED_LAST_DELTA_REPLICA_STA { get; set; }
        [Column("SHIPPEDLASTDELTAREPLICAEND")]
        public string SHIPPED_LAST_DELTA_REPLICA_END { get; set; }
        [Column("ASYNCBUFFERFULLCOUNT")]
        public string ASYNC_BUFFER_FULL_COUNT { get; set; }
        [Column("BACKLOGSIZE")]
        public string BACKLOG_SIZE { get; set; }
        [Column("MAXBACKLOGSIZE")]
        public string MAX_BACKLOG_SIZE { get; set; }
        [Column("BACKLOGTIME")]
        public string BACKLOG_TIME { get; set; }
        [Column("MAXBACKLOGTIME")]
        public string MAX_BACKLOG_TIME { get; set; }
        public int CustomerId { get; set; }
        public int ProductId { get; set; }

        public Guid SREQINDEX { get; set; }

    }
}
