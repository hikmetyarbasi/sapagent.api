﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SapAgent.API.Model
{
    public class SmsRequest
    {
        public string phone { get; set; }
        public string message { get; set; }
    }

    public class SmsResponse
    {
        public Body body { get; set; }
    }

    public class Body
    {
        public int smsServiceResult { get; set; }
    }
}
