﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SapAgent.Entities.Concrete.General.@enum
{
    public enum ReplicEnumCase
    {
        AppServerDown = 1,
        MissingServerState = 2
    }
}
