﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using SapAgent.Entities.Abstract;

namespace SapAgent.Entities.Concrete.Admin
{
    public class TableDataCount: IEntity
    {
        [Key]
        public Int64 Id { get; set; }
        public string SchemaName { get; set; }
        public Int64 DataCount { get; set; }
        public string FunctionName { get; set; }
    }
}
