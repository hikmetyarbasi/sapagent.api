﻿namespace SapAgent.ExternalServices
{
    public class AppSettings
    {
        public string ConnectionStringSapAgent { get; set; }
        public bool IsAllowSendSms { get; set; }
        public string[] SmsReceiverList { get; set; }
        public string[] EmailReceiverList { get; set; }
        public string Environment { get; set; }
    }
}
