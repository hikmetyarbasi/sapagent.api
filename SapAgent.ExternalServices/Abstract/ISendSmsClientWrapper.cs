﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SapAgent.API.Model;
using SmsService;

namespace SapAgent.ExternalServices.Abstract
{
    public interface ISendSmsClientWrapper
    {
        Task<SmsServiceResponse> SendSms(SmsRequest smsRequest);
    }
}
