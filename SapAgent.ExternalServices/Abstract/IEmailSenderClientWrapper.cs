﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SapAgent.ExternalServices.Abstract
{
    public interface IEmailSenderClientWrapper
    {
        void SendEmail(string content);
    }
}
