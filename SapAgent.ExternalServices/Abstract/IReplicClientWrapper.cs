﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PrdReplic;

namespace SapAgent.ExternalServices.Abstract
{
    public interface IReplicClientWrapper
    {

        Task<ZAYGBCGEN_HSYSW_REPLIC_OUTSTRU[]> GetData();
    }
}
