﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using SapAgent.ExternalServices.Abstract;

namespace SapAgent.ExternalServices.Concrete
{

    public class EmailSenderClientWrapper : IEmailSenderClientWrapper
    {
        private AppSettings _config;
        public EmailSenderClientWrapper(AppSettings config)
        {
            _config = config;
        }
        private static readonly string fromadres = "sapagent@aygaz.com.tr";
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void SendEmail(string content)
        {
            var msg = new MailMessage();
            msg.From = new MailAddress(fromadres);
            foreach (var receiver in _config.EmailReceiverList)
            {
                msg.To.Add(new MailAddress(receiver));
            }
            msg.Subject = "Sap Agent System Notification";
            msg.Body = content;
            msg.IsBodyHtml = true;
            var client = new SmtpClient();
            client.Host = "smtp.aygaznet.local";
            try
            {
                client.Send(msg);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }
    }
}
