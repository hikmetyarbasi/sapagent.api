﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using PrdReplic;
using SapAgent.ExternalServices.Abstract;

namespace SapAgent.ExternalServices.Concrete
{
    public class ReplicClientWrapper : IReplicClientWrapper
    {
        private readonly ZAYGBCGEN_WS_REPLIC_CClient _client;

        public ReplicClientWrapper()
        {
            BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.TransportCredentialOnly);
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            EndpointAddress endpoint = new EndpointAddress("http://AYGERPPRD.AYGAZNET.LOCAL:8000/sap/bc/srt/rfc/sap/zaygbcgen_ws_replic_c/400/zaygbcgen_ws_replic_c_srv/zaygbcgen_ws_replic_c_bn");

            _client = new ZAYGBCGEN_WS_REPLIC_CClient(binding, endpoint);
            _client.ClientCredentials.UserName.UserName = "Rfc_etalep";
            _client.ClientCredentials.UserName.Password = "Temp2010";
        }
        public async Task<ZAYGBCGEN_HSYSW_REPLIC_OUTSTRU[]> GetData()
        {
            var data = await _client.ZAYGBCGEN_HSYSW_REPLICAsync(new ZAYGBCGEN_HSYSW_REPLIC());
            return data.ZAYGBCGEN_HSYSW_REPLICResponse.ET_SERV_REPLICATION_C;
        }
    }
}
