﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using SapAgent.API.Model;
using SapAgent.ExternalServices.Abstract;
using SmsService;
namespace SapAgent.ExternalServices.Concrete
{
    public class SendSmsClientWrapper : ISendSmsClientWrapper
    {
        sapSoapClient _client;
        private AppSettings _config;
        public SendSmsClientWrapper(AppSettings config)
        {
            _config = config;
            BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.TransportCredentialOnly);
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            EndpointAddress endpoint = new EndpointAddress("http://etalepwebservis.aygaz.com.tr/sap.asmx");

            _client = new sapSoapClient(binding, endpoint);
            _client.ClientCredentials.UserName.UserName = "a";
            _client.ClientCredentials.UserName.Password = "a";
        }

        public bool IsAllowSms => _config.IsAllowSendSms;
        public async Task<SmsServiceResponse> SendSms(SmsRequest smsRequest)
        {
            SmsServiceResponse response = null;
            try
            {

                if (IsAllowSms)
                {
                    foreach (var phone in _config.SmsReceiverList)
                    {
                        response = await _client.SmsServiceAsync(phone, smsRequest.message, "1");
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return response;
        }
    }
}
