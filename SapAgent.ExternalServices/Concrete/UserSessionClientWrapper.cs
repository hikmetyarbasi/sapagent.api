﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using SapAgent.ExternalServices.Abstract;
using PrdUserSession;

namespace SapAgent.ExternalServices.Concrete
{
    public class UserSessionClientWrapper:IUserSessionClientWrapper
    {
        private zaygbcsys_ws_usersesClient _userSessionClient;

        public UserSessionClientWrapper()
        {

            BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.TransportCredentialOnly);
            binding.MaxBufferSize = 20000000;
            binding.MaxReceivedMessageSize = 20000000;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            EndpointAddress endpoint = new EndpointAddress("http://AYGERPPRD.AYGAZNET.LOCAL:8000/sap/bc/srt/rfc/sap/zaygbcsys_ws_userses/400/zaygbcsys_ws_userses/zaygbcsys_ws_userses_bn");

            var _client = new zaygbcsys_ws_usersesClient(binding, endpoint);
            _client.ClientCredentials.UserName.UserName = "Rfc_etalep";
            _client.ClientCredentials.UserName.Password = "Temp2010";
            this._userSessionClient = _client;

          }
        public async Task<ZaygbssysUsersessRf[]> GetData()
        {
            try
            {
                var data = await _userSessionClient.ZaygbcsysRfcsUsersesAsync(new ZaygbcsysRfcsUserses());
                return data.ZaygbcsysRfcsUsersesResponse.EtUsrSessList;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
