﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Helpers.Abstract;
using Helpers.Concrete;
using SapAgent.API.Model;
using SapAgent.Business.Config.Abstract;
using SapAgent.DataAccess.Abstract;
using SapAgent.Entities.Concrete.Config;
using SapAgent.Entities.Concrete.General;
using SapAgent.Entities.Concrete.General.@enum;
using SapAgent.Entities.Concrete.Spa;
using INotificationSender = SapAgent.Business.Admin.Abstract.INotificationSender;

namespace SapAgent.Business.Config.Concrete
{
    public class ConfigReplicManager : ConfigManager<Replic>, IConfigReplicManager
    {

        private new const int FunctionId = 1008;
        private const int CustomerId = 1;
        private const int ProductId = 1;

        private Guid lstExeTime;
        // private readonly IHttpClientHelper<DashboardSignalRModel> _httpClientHelper;
        private IBaseDal<Entities.Concrete.Pure.Replic> _basePureDal;
        private readonly IBaseDal<ReplicNotify> _notificationDal;
        private readonly IBaseDal<CustomerProductView> _customerProdDal;
        private readonly IBaseDal<ReplicNotifyDetailView> _notifyDetailDal;
        private readonly IHttpClientHelper<SmsRequest, SmsResponse> _httpClientSmsHelper;
        private INotificationSender notificationSender;
        public ConfigReplicManager(
            IBaseDal<Replic> entityRepository,
            IBaseDal<FuncFlag> flagDal,
            IBaseDal<Entities.Concrete.Pure.Replic> basePureDal,
            IBaseDal<ReplicNotify> notificationDal,
            IBaseDal<CustomerProductView> customerProdDal,
            IBaseDal<ReplicNotifyDetailView> notifyDetailDal,
            IBaseDal<AllNotifyCountView> allNotifyDal, IHttpClientHelper<SmsRequest, SmsResponse> httpClientSmsHelper, INotificationSender notificationSender)
            : base(entityRepository, flagDal, FunctionId)
        {
            _basePureDal = basePureDal;
            _notificationDal = notificationDal;
            _customerProdDal = customerProdDal;
            _notifyDetailDal = notifyDetailDal;
            _httpClientSmsHelper = httpClientSmsHelper;
            this.notificationSender = notificationSender;
            _customerProdDal = customerProdDal;
        }

        public override void StartOperation(int customerId, int productId)
        {
            try
            {
                if (IsFlagUp())
                {
                    lstExeTime = GetLastExecutionIndex();

                    var rawData = _basePureDal.GetAll(x => x.SREQINDEX == lstExeTime).Result;
                    var alertlist = CatchAlert(rawData);

                    //Triggered SignalR
                    if (alertlist.Count > 0)
                    {


                        AddNotificationToDb(alertlist);

                        //_httpClientHelper.PostRequest("Message/api/dashboardUpdate", new DashboardSignalRModel());
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }

        private List<ReplicNotify> CatchAlert(List<Entities.Concrete.Pure.Replic> rawData)
        {
            var list = new List<ReplicNotify>();
            var pastsreqIndex = GetPastExecutionIndex();
            var savedHosts = _basePureDal.GetAll(x => x.SREQINDEX == pastsreqIndex).Result.ToList();
            var clientList = _customerProdDal.GetAll(x => x.ProductId == ProductId).Result.ToList();
            foreach (var item in rawData)
            {
                if (item.REPLICATION_STATUS != "ACTIVE")
                {
                    foreach (var client in clientList)
                    {
                        list.Add(new ReplicNotify()
                        {
                            FuncId = FunctionId,
                            Desc = item.HOST + " sunucusunun durumu " + item.REPLICATION_STATUS + " durumuna geçmiştir.",
                            Case = (int)ReplicEnumCase.AppServerDown,
                            Date = DateTime.Now,
                            Level = (int)Level.error,
                            CustomerProductId = GetCustomerProductId(Convert.ToInt32(client.ClientId)),
                            Statu = 0
                        });
                    }
                }

            }
            foreach (var host in savedHosts)
            {
                var entity = rawData.FirstOrDefault(x => x.HOST == host.HOST);
                if (entity == null)
                {
                    foreach (var client in clientList)
                    {
                        list.Add(new ReplicNotify()
                        {
                            FuncId = FunctionId,
                            Desc = host.HOST + " sunucusunun durum bilgisi gelmemiştir.",
                            Case = (int)SysListEnumCase.MissingServerState,
                            Date = DateTime.Now,
                            Level = (int)Level.error,
                            CustomerProductId = GetCustomerProductId(Convert.ToInt32(client.ClientId)),
                            Statu = 0
                        });
                    }
                }
            }
            return list;
        }

        private async void AddNotificationToDb(List<ReplicNotify> alertlist)
        {
            foreach (var item in alertlist)
            {
                if (item.Level == (int)Level.error)
                {
                    var smscontent = "Replic --> " + item.Desc;
                    notificationSender.SendNotificationAsync(smscontent);
                }

                _notificationDal.Add(item);
            }
        }
        private int GetCustomerProductId(int clientId)
        {
            return _customerProdDal.Get(x => x.CustomerId == CustomerId && x.ProductId == ProductId && x.ClientId == clientId).CustomerProductId;
        }
    }
}
