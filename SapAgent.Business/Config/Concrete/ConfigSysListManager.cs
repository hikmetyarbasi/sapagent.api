﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Helpers.Abstract;
using Helpers.Concrete;
using SapAgent.API.Model;
using SapAgent.Business.Config.Abstract;
using SapAgent.DataAccess.Abstract;
using SapAgent.Entities.Concrete;
using SapAgent.Entities.Concrete.Config;
using SapAgent.Entities.Concrete.General;
using SapAgent.Entities.Concrete.General.@enum;
using SapAgent.Entities.Concrete.Spa;
using INotificationSender = SapAgent.Business.Admin.Abstract.INotificationSender;

namespace SapAgent.Business.Config.Concrete
{
    public class ConfigSysListManager : ConfigManager<SysList>, IManagerConfigSysListManager
    {
        private const int FunctionId = 4;
        private const int CustomerId = 1;
        private const int ProductId = 1;

        private Guid lstExeTime;
        // private readonly IHttpClientHelper<DashboardSignalRModel> _httpClientHelper;
        private IBaseDal<Entities.Concrete.Pure.SysList> _basePureDal;
        private readonly IBaseDal<SysListNotify> _notificationDal;
        private readonly IBaseDal<CustomerProductView> _customerProdDal;
        private readonly IBaseDal<SysListNotifyDetailView> _notifyDetailDal;
        private readonly IHttpClientHelper<SmsRequest, SmsResponse> _httpClientSmsHelper;
        private readonly INotificationSender notificationSender;
        public ConfigSysListManager(IBaseDal<SysList> entityRepository,
            IBaseDal<FuncFlag> flagDal,
            //  IHttpClientHelper<DashboardSignalRModel> httpClientHelper,
            IBaseDal<Entities.Concrete.Pure.SysList> basePureDal,
            IBaseDal<SysListNotify> notificationDal,
            IBaseDal<CustomerProductView> customerProdDal,
            IBaseDal<SysListNotifyDetailView> notifyDetailDal,
            IBaseDal<AllNotifyCountView> allNotifyDal, IHttpClientHelper<SmsRequest, SmsResponse> httpClientSmsHelper, INotificationSender notificationSender)
            : base(entityRepository, flagDal, FunctionId)
        {
            //_httpClientHelper = httpClientHelper;
            _basePureDal = basePureDal;
            _notificationDal = notificationDal;
            _customerProdDal = customerProdDal;
            _notifyDetailDal = notifyDetailDal;
            _httpClientSmsHelper = httpClientSmsHelper;
            this.notificationSender = notificationSender;
            _customerProdDal = customerProdDal;
        }

        public async Task<List<SysListNotifyDetailView>> GetSysListNotifyDetail(int customerProductId)
        {
            return await _notifyDetailDal.GetAll(x => x.CustomerProductId == customerProductId);
        }

        public override void StartOperation(int customer, int productId)
        {
            try
            {
                if (IsFlagUp())
                {
                    lstExeTime = GetLastExecutionIndex();
                    var rawData = _basePureDal.GetAll(x => x.SREQINDEX == lstExeTime).Result;
                    var alertlist = CatchAlert(rawData);

                    //Triggered SignalR
                    if (alertlist.Count > 0)
                    {
                        AddNotificationToDb(alertlist);

                        //_httpClientHelper.PostRequest("Message/api/dashboardUpdate", new DashboardSignalRModel());
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }

        private async void AddNotificationToDb(List<SysListNotify> alertlist)
        {
            foreach (var item in alertlist)
            {
                if (item.Level == (int)Level.error)
                {
                    var smscontent = "SM51 --> " + item.Desc;
                    notificationSender.SendNotificationAsync(smscontent);
                }

                _notificationDal.Add(item);
            }
        }

        private List<SysListNotify> CatchAlert(List<Entities.Concrete.Pure.SysList> rawData)
        {
            try
            {
                var listNotify = new List<SysListNotify>();
                var pastsreqIndex = GetPastExecutionIndex();
                var savedHosts = _basePureDal.GetAll(x => x.SREQINDEX == pastsreqIndex).Result.ToList();

                var clientList = _customerProdDal.GetAll(x => x.ProductId == ProductId).Result.ToList();
                foreach (var item in rawData)
                {
                    if (item.STATUS == "Down")
                    {
                        foreach (var client in clientList)
                        {
                            listNotify.Add(new SysListNotify()
                            {
                                FuncId = FunctionId,
                                Desc = item.HOST + " sunucusunun durumu " + item.STATUS + " durumuna geçmiştir.",
                                Case = (int)SysListEnumCase.AppServerDown,
                                Date = DateTime.Now,
                                Level = (int)Level.error,
                                CustomerProductId = GetCustomerProductId(Convert.ToInt32(client.ClientId)),
                                Statu = 0
                            });
                        }
                    }

                    //_baseConfigDal.Add(new SysList()
                    //{
                    //    ProductId = ProductId,
                    //    CustomerId = CustomerId,
                    //    Host = item.HOST,
                    //    Status = item.STATUS
                    //});

                }

                foreach (var host in savedHosts)
                {
                    var entity = rawData.FirstOrDefault(x => x.HOST == host.HOST);
                    if (entity == null)
                    {
                        foreach (var client in clientList)
                        {
                            listNotify.Add(new SysListNotify()
                            {
                                FuncId = FunctionId,
                                Desc = host.HOST + " sunucusunun durum bilgisi gelmemiştir.",
                                Case = (int)SysListEnumCase.MissingServerState,
                                Date = DateTime.Now,
                                Level = (int)Level.error,
                                CustomerProductId = GetCustomerProductId(Convert.ToInt32(client.ClientId)),
                                Statu = 0
                            });
                        }
                    }
                }

                return listNotify;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }
        private int GetCustomerProductId(int clientId)
        {
            return _customerProdDal.Get(x => x.CustomerId == CustomerId && x.ProductId == ProductId && x.ClientId == clientId).CustomerProductId;
        }
    }
}
