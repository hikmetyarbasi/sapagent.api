﻿using System.Threading.Tasks;
using SapAgent.Entities.Concrete.Pure;

namespace SapAgent.Business.Pure.Abstract
{
    public interface IManagerUserSession:IManager<Entities.Concrete.Pure.UserSession>
    {
    }
}