﻿using System;
using System.Collections.Generic;
using System.Text;
using Helpers.Abstract;
using SapAgent.Business.Pure.Abstract;
using SapAgent.DataAccess.Abstract;
using SapAgent.Entities.Concrete.Config;
using SapAgent.Entities.Concrete.Pure;

namespace SapAgent.Business.Pure.Concrete
{
    public class ReplicManager : Manager<Entities.Concrete.Pure.Replic>, IManagerReplic
    {

        private new const int FunctionId = 1008;
        public ReplicManager(IBaseDal<Entities.Concrete.Pure.Replic> entityRepository, IHttpClientHelper<int, Entities.Concrete.Pure.Replic> httpClient, IBaseDal<FuncFlag> funcFlagBaseDal) : base(entityRepository, httpClient, FunctionId, funcFlagBaseDal)
        {
        }
    }
}
