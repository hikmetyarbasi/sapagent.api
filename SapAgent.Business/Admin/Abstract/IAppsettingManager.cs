﻿using System;
using System.Collections.Generic;
using System.Text;
using SapAgent.ExternalServices;

namespace SapAgent.Business.Admin.Abstract
{
    public interface IAppsettingManager
    {
        AppSettings Get();
    }
}
