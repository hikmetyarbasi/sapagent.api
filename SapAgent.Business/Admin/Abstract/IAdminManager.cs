﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using SapAgent.Entities.Concrete.Admin;

namespace SapAgent.Business.Admin.Abstract
{
    public interface IAdminManager<T>
    {
        T Add(T entity);
        void Update(T entity);
        List<T> GetAll(Expression<Func<T, bool>> filter);
        void ExecuteSqlQuery(string sql);
        void Upsert(T entity);
        List<TableDataCount> ExecTableDataCountSp(string sp);
    }
}
