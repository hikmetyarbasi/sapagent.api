﻿using System.Threading.Tasks;

namespace SapAgent.Business.Admin.Abstract
{
    public interface INotificationSender
    {
        Task SendNotificationAsync(string content);
    }
}
