﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using SapAgent.Business.Admin.Abstract;
using SapAgent.Entities.Concrete.Admin;
using SapAgent.ExternalServices;

namespace SapAgent.Business.Admin.Concrete
{
    public class AppSettingManager : IAppsettingManager
    {
        private readonly AppSettings appSettings;
        public AppSettingManager(AppSettings appSettings)
        {
            this.appSettings = appSettings;
        }

        public AppSettings Get()
        {
            return appSettings;
        }
    }
}
