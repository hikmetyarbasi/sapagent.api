﻿using System.Threading.Tasks;
using Helpers.Concrete;
using SapAgent.API.Model;
using SapAgent.ExternalServices.Abstract;
using INotificationSender = SapAgent.Business.Admin.Abstract.INotificationSender;

namespace SapAgent.Business.Admin.Concrete
{
    public class NotificationSender : INotificationSender
    {
        private ISendSmsClientWrapper _smsSender;
        private IEmailSenderClientWrapper _emailSender;
        public NotificationSender(ISendSmsClientWrapper smsSender, IEmailSenderClientWrapper emailSender)
        {
            _smsSender = smsSender;
            _emailSender = emailSender;
        }
        public async Task SendNotificationAsync(string content)
        {
            await _smsSender.SendSms(new SmsRequest(){message = content });
            _emailSender.SendEmail(content);
        }

    }
}
