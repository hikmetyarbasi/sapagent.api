﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SapAgent.Business.Admin.Abstract;
using SapAgent.DataAccess.Abstract;
using SapAgent.DataAccess.Concrete.EntityFramework;
using SapAgent.Entities.Abstract;
using SapAgent.Entities.Concrete.Admin;

namespace SapAgent.Business.Admin.Concrete
{
    public class AdminManager<T> : IAdminManager<T> where T : class, IEntity, new()
    {
        private readonly IBaseDal<T> _entityRepository;
        private SapAgentContext _context;
        public AdminManager(IBaseDal<T> entityRepository, SapAgentContext context)
        {
            _entityRepository = entityRepository;
            this._context = context;
        }
        public T Add(T entity)
        {
            return _entityRepository.Add(entity);
        }

        public void Update(T entity)
        {
            _entityRepository.Update(entity);
        }

        public List<T> GetAll(Expression<Func<T, bool>> filter)
        {
            return _entityRepository.GetAll(filter).Result;
        }

        public void ExecuteSqlQuery(string sql)
        {

            _entityRepository.ExecuteSqlQuery(sql);
        }

        public void Upsert(T entity)
        {
            _entityRepository.Upsert(entity);
        }
        public List<TableDataCount> ExecTableDataCountSp(string sp)
        {
            return _context.TableDataCounts.FromSql(sp).ToList();
        }
    }
}
