﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SapAgent.API.Model;
using SapAgent.Business.Admin.Abstract;
using SapAgent.Entities.Concrete.Admin;
using SapAgent.ExternalServices;
using SapAgent.ExternalServices.Abstract;
using SapAgent.ExternalServices.Concrete;
using SmsService;

namespace SapAgent.API.Controllers
{
    [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {

        private readonly IAdminManager<TableDataCount> _tableDataCount;
        private readonly IAppsettingManager _appsettings;

        public AdminController(IAdminManager<TableDataCount> tableDataCount,
                               IAppsettingManager appsettings)
        {
            _tableDataCount = tableDataCount;
            _appsettings = appsettings;
        }
        [Route("tablerowcount")]
        [HttpGet]
        public List<TableDataCount> GetTableRowCount()
        {
            return _tableDataCount.ExecTableDataCountSp("SP_TABLESROW_COUNT");
        }
        //[Route("sendsms")]
        //[HttpPost]
        //public SmsServiceResponse SendSms(SmsRequest smsRequest)
        //{
        //    return _smsSender.SendSms(smsRequest).Result;
        //}
        [Route("getagentappSettings")]
        [HttpPost]
        public AppSettings GetAgentAppSettings()
        {
            return _appsettings.Get();
        }
    }
}