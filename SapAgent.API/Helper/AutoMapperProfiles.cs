﻿using AutoMapper;
using PrdBackgroundProcess;
using PrdCheckDumps;
using PrdCheckLocks;
using PrdKernalCompat;
using PrdReplic;
using PrdSystemFile;
using PrdSystemList;
using PrdSystemUsage;
using PrdUserSession;
using SapAgent.Entities.Concrete.Pure;

namespace SapAgent.API.Helper
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<ZaygbssysTbtcjobBkRfChar, BackgroundProcess>();
            CreateMap<ZaygbcsysRdumpov, Dump>();
            CreateMap<ZaygbcsysLocksRf, Lock>();
            CreateMap<ZaygbcsysMsxxlistV6Rf, SysList>();
            CreateMap<CcmFsysSingle, SysFile>();
            CreateMap<ZaygbcsysKernelstatRf, KernelCompat>();

            CreateMap<ZaygbssysUsersessRf, UserSession>();
            CreateMap<CcmSnapAll, SystemUsageTcpu>().ForMember(dest=>dest.TYPE, opt =>
            {
                opt.MapFrom(x=>"Cpu");
            });
            CreateMap<CcmSnapAll, SystemUsageTmem>().ForMember(dest => dest.TYPE, opt =>
            {
                opt.MapFrom(x => "Mem");
            });
            CreateMap<SystemUsageTmem, SysUsage>();
            CreateMap<SystemUsageTcpu, SysUsage>();
            CreateMap<ZAYGBCGEN_HSYSW_REPLIC_OUTSTRU, Replic>();
        }
    }
}
